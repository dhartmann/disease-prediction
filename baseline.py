__author__ = 'David'

import csv
import random

class Colors:
    HEADER  = '\033[95m'
    INFO    = '\033[94m'
    SUCCESS = '\033[92m'
    WARNING = '\033[93m'
    ERROR   = '\033[91m'
    OTHER   = '\033[96m'
    ENDC    = '\033[0m'

reader = csv.reader(open("data/tigers.csv", mode='rU'))
tiger_data = dict((rows[0], {"sex": rows[1], "disease": rows[2], "institution": rows[3]}) for rows in reader)
del tiger_data['STUDBK']

test_diseases = [
    'BONE & JOINT DISEASE',
    'NEOPLASIA'
]

# 1: male
# 0: female


counters = {}
totals = {}
for d in test_diseases:
    counters[d] = {}
    for t in tiger_data:
        counters[d][tiger_data[t]["institution"]] = 0
for d in test_diseases:
    totals[d] = 0

for d in test_diseases:
    for t in tiger_data:
        if tiger_data[t]["disease"] == d:
            totals[d] += 1
            counters[d][tiger_data[t]["institution"]] += 1

print counters

probs = {}
for d in test_diseases: probs[d] = []

for d in counters:
    for inst in counters[d]:
        # probs[d][inst] = counters[d][inst] / float(totals[d])
        probs[d].append((inst, counters[d][inst] / float(totals[d])))
    probs[d].sort(key=lambda x: -x[1])


print probs

threshold = 0.5
tolerance = 0.5
test_tigers = []
passed = 0
failed = 0
disease_passed = [0, 0]
disease_failed = [0, 0]

has_disease = 0
guess = 0
correct_guess = 0
print len(tiger_data)
for i in range(len(tiger_data) / 10):
    tiger = random.choice(tiger_data.keys())
    test_tigers.append(tiger)
    del tiger_data[tiger]

total_deviation = 0.0
for tiger in test_tigers:
    for disease in test_diseases:
        prob = 0
        actual_prob = 1 if tiger_data[t]["disease"] == disease else 0
        for inst, probability in probs[d]:
            count = 0.0
            n = 1
            for i in range(n):
                if random.random() < probability:
                    # count += 1
                    prob = 1
            # prob = count / n
        deviation = abs(actual_prob - prob)

        if actual_prob > threshold:
            # count disease occurrences
            has_disease += 1
        if prob > threshold:
            # count guesses
            guess += 1
            if actual_prob > threshold:
                correct_guess += 1

        if deviation <= tolerance:
            status = "PASSED"
            color = Colors.SUCCESS
            passed += 1
            disease_passed[int(actual_prob)] += 1
        else:
            status = "FAILED"
            color = Colors.ERROR
            failed += 1
            disease_failed[int(actual_prob)] += 1
        # if self.verbose and not (self.fail_only and deviation <= self.tolerance):
        print "{}: {} \testimated {:.3f} to have {: <20} \tActual {:.3f} \twith deviation {:.3f}".format(
                status, tiger, prob, disease, actual_prob, deviation)
        total_deviation += deviation

        print Colors.INFO, "Precision:\t", correct_guess, "/", guess, '=', float(correct_guess)/max(guess, 0.00001), Colors.ENDC
        print Colors.INFO, "Recall:\t", correct_guess, "/", has_disease, '=', float(correct_guess)/max(has_disease, 0.00001), Colors.ENDC