"""
Calculates node probability
"""
__author__ = 'David'

import csv
import random
import math

class Colors:
    HEADER  = '\033[95m'
    INFO    = '\033[94m'
    SUCCESS = '\033[92m'
    WARNING = '\033[93m'
    ERROR   = '\033[91m'
    OTHER   = '\033[96m'
    ENDC    = '\033[0m'


class Predictor:
    def __init__(self, tigers_path, disease_path):
        self.tigers_path = tigers_path
        self.disease_path = disease_path
        self.primary_only = True
        self.similarities = {}
        self.weights = {}
        self.tigers = []
        self._load_tigers()
        self.disease_probability = {}

        self.diseases = []
        self._load_diseases()
        self._load_disease_probabilities()

    def _load_tigers(self):
        reader = csv.reader(open(self.tigers_path, mode='r'))
        data = dict((rows[0], rows[1:]) for rows in reader)
        self.tigers = data['']

    def _load_diseases(self):
        reader = csv.reader(open(self.disease_path, mode='r'))
        temp_disease_data = dict(((rows[0], rows[2]), rows[1]) for rows in reader)
        del temp_disease_data[('STUDBK', 'DP_LUMPED')]

        for tiger, disease in temp_disease_data:
            if disease not in self.diseases:
                self.diseases.append(disease)

    def _load_disease_probabilities(self):
        reader = csv.reader(open(self.disease_path, mode='r'))
        temp_disease_data = dict(((rows[0], rows[2]), rows[1]) for rows in reader)
        del temp_disease_data[('STUDBK', 'DP_LUMPED')]

        # initializes all disease probabilities with 0
        self.disease_probability = dict(
            (tiger, dict((disease, 0) for disease in self.diseases)) for tiger in self.tigers)

        for tiger, disease in temp_disease_data:
            if tiger in self.tigers:
                disease_weight = temp_disease_data[(tiger, disease)]
                if disease_weight == '' or (self.primary_only and float(disease_weight) < 10):
                    self.disease_probability[tiger][disease] = 0
                else:
                    self.disease_probability[tiger][disease] = float(disease_weight) / 10.0
                    # normalize probabilities of diseases (divide by sum of them

    def add_similarity(self, similarity, weight):
        self.similarities[similarity] = {}
        self.weights[similarity] = weight
        reader = csv.reader(open('data/similarities/' + similarity + '.csv', mode='rU'))
        data = dict((rows[0], rows[1:]) for rows in reader)
        local_tigers = data['']
        del data['']

        for tiger in self.tigers:
            self.similarities[similarity][tiger] = {}
            for index in range(len(local_tigers)):
                self.similarities[similarity][tiger][local_tigers[index]] = float(data[tiger][index] or 0)

    def sim(self, t1, t2):
        """
        Determines similarity between t1 and t2 over all similarity values in sims
        :param t1: origin Tiger t1
        :param t2: Tiger t2 to compare Tiger t1 to
        :return: combined similarity over all similarities
        """
        summed_sims = 0
        for s in self.similarities:
            summed_sims += self.weights[s] * self.sim_s(s, t1, t2)
        return 1. / self.sum_of_weights() * summed_sims


    def sim_s(self, s, t1, t2):
        """
        Calculates the similarity between two elements with respect to similarity s from similarities sims
        :param s: string similarity to measure similarity of tigers t1 and t2 of
        :param t1: origin Tiger t1
        :param t2: Tiger t2 to compare Tiger t1 to with respect to similarity s
        :rtype : float
        :return: Returns similarity between two elements with respect to similarity s from similarities sims
        """
        return self.similarities[s][t1][t2]


    def sum_of_weights(self):
        """
        Returns the sum of all weights of similarities
        :rtype : float
        :return: sum of all weights
        """
        total = 0
        for s in self.similarities:
            total += self.weights[s]
        return total

    def calculate_disease_probability(self, tiger, disease):
        sum_sims_probs = 0
        for t in self.tigers:
            if t == tiger:
                continue
            sim = self.sim(tiger, t)
            prob = self.disease_probability[t][disease]
            if sim > 0 or prob > 0:
                # print "Sim", sim, "Prob", prob
                pass
            sum_sims_probs += self.sim(tiger, t) * self.disease_probability[t][disease]
        alpha = self.alpha(tiger)
        return alpha * sum_sims_probs

    def alpha(self, tiger):
        """

        :param tiger:
        :rtype : float
        :return:
        """
        sum_sims = 0
        for t in self.tigers:
            if t == tiger:
                continue
            sum_sims += self.sim(tiger, t)
        return 1. / sum_sims if sum_sims > 0 else 1000000000


# Checks
class Check:
    """
    Check similarities against testing ones.
    """
    sample_data = {"24": {"20": 0.875,
                          "130": 0.0,
                          "183": 0.3333,
                          "232": 0.3333},
                   "230": {"20": 0.3333}}

    def __init__(self, predictor, test_diseases, tolerance=0.1, threshold=0.5, verbose=False, fail_only=False):
        self.tolerance = tolerance
        self.threshold = threshold
        self.verbose = verbose
        self.fail_only = fail_only
        self.predictor = predictor
        self.test_diseases = test_diseases

        self.total_passed = 0
        self.total_failed = 0
        self.total_disease_passed = [0, 0]
        self.total_disease_failed = [0, 0]

        self.total_has_disease = 0
        self.total_guess = 0
        self.total_correct_guess = 0

    def check_sim(self, t1, t2):
        """
        Check if similarity of t1 and t2 matches sample data.
        :param t1: Tiger t1
        :param t2: Tiger t2
        """
        s = self.predictor.sim(t1, t2)
        if abs(self.predictor.sim(t1, t2) - self.sample_data[t1][t2]) < self.tolerance:
            print Colors.SUCCESS, "Tigers {} and {} have the correct similarity of {}".format(t1, t2, s), Colors.ENDC
        else:
            print Colors.ERROR, "Similarity {} of {} and {} is off. Should be: {}".format(s, t1, t2, self.sample_data[t1][t2]), Colors.ENDC

    def run_checks(self):
        """
        Runs checks on similarity for all sample data.
        """
        for t1 in self.sample_data:
            for t2 in self.sample_data[t1]:
                self.check_sim(t1, t2)

    def tenfold(self):
        self.total_passed = 0
        self.total_failed = 0
        self.total_disease_passed = [0, 0]
        self.total_disease_failed = [0, 0]

        self.total_has_disease = 0
        self.total_guess = 0
        self.total_correct_guess = 0

        test_sum = sum(self.tenth_test() for i in range(10))
        if self.verbose:
            print
            print Colors.HEADER, "TOTALS", Colors.ENDC
            print Colors.INFO, "Total PASSED", self.total_passed, "Total FAILED", self.total_failed, Colors.ENDC
            print Colors.INFO, "Total No Disease:", self.total_disease_passed[0], "/", (self.total_disease_passed[0] + self.total_disease_failed[0]), Colors.ENDC
            print Colors.INFO, "Total Disease:   ", self.total_disease_passed[1], "/", (self.total_disease_passed[1] + self.total_disease_failed[1]), Colors.ENDC
            print
            print Colors.INFO, "Total Precision:\t", self.total_correct_guess, "/", self.total_guess, '=', float(self.total_correct_guess)/max(self.total_guess, 0.00001), Colors.ENDC
            print Colors.INFO, "Total Recall:\t\t", self.total_correct_guess, "/", self.total_has_disease, '=', float(self.total_correct_guess)/max(self.total_has_disease, 0.00001), Colors.ENDC
        return test_sum / 10

    def tenth_test(self):
        test_tigers = []
        passed = 0
        failed = 0
        disease_passed = [0, 0]
        disease_failed = [0, 0]

        has_disease = 0
        guess = 0
        correct_guess = 0

        for i in range(len(self.predictor.tigers) / 10):
            tiger = random.choice(self.predictor.tigers)
            test_tigers.append(tiger)
            self.predictor.tigers.remove(tiger)

        total_deviation = 0.0
        for tiger in test_tigers:
            for disease in self.test_diseases:
                actual_prob = self.predictor.disease_probability[tiger][disease]
                prob = self.predictor.calculate_disease_probability(tiger, disease)
                deviation = abs(actual_prob - prob)

                if actual_prob > self.threshold:
                    # count disease occurrences
                    has_disease += 1
                if prob > self.threshold:
                    # count guesses
                    guess += 1
                    if actual_prob > self.threshold:
                        correct_guess += 1

                if deviation <= self.tolerance:
                    status = "PASSED"
                    color = Colors.SUCCESS
                    passed += 1
                    disease_passed[int(actual_prob)] += 1
                else:
                    status = "FAILED"
                    color = Colors.ERROR
                    failed += 1
                    disease_failed[int(actual_prob)] += 1
                if self.verbose and not (self.fail_only and deviation <= self.tolerance):
                    print color, "{}: {} \testimated {:.3f} to have {: <20} \tActual {:.3f} \twith deviation {:.3f}".format(
                        status, tiger, prob, disease, actual_prob, deviation), Colors.ENDC
                total_deviation += deviation

        self.predictor.tigers += test_tigers
        self.total_passed += passed
        self.total_failed += failed
        self.total_disease_passed[0] += disease_passed[0]
        self.total_disease_passed[1] += disease_passed[1]
        self.total_disease_failed[0] += disease_failed[0]
        self.total_disease_failed[1] += disease_failed[1]

        self.total_has_disease += has_disease
        self.total_guess += guess
        self.total_correct_guess += correct_guess

        if self.verbose:
            print Colors.INFO, "PASSED:", passed, "FAILED:", failed, Colors.ENDC
            print Colors.INFO, "No Disease:", disease_passed[0], "/", (disease_passed[0] + disease_failed[0]), Colors.ENDC
            print Colors.INFO, "Disease:   ", disease_passed[1], "/", (disease_passed[1] + disease_failed[1]), Colors.ENDC

            print Colors.INFO, "Precision:\t", correct_guess, "/", guess, '=', float(correct_guess)/max(guess, 0.00001), Colors.ENDC
            print Colors.INFO, "Recall:\t", correct_guess, "/", has_disease, '=', float(correct_guess)/max(has_disease, 0.00001), Colors.ENDC
        return total_deviation / len(test_diseases) / len(test_tigers)


# predictor = Predictor(tigers_path='data/similarities/test_sex.csv', disease_path='data/test_disease_dense.csv')
# predictor = Predictor(tigers_path='data/similarities/test_sex.csv', disease_path='data/test_disease_sparse.csv')
# predictor = Predictor(tigers_path='data/similarities/sex.csv', disease_path='data/disease.csv')
predictor = Predictor(tigers_path='data/similarities/sex.csv', disease_path='data/disease_primary.csv')

predictor.tigers.remove('3300')
predictor.tigers.remove('4455')
predictor.tigers.remove('T9313')
predictor.tigers.remove('5199')

similarities = [
    {'name': 'relatedness', 'weight': 1},
    # {'name': 'institution', 'weight': 3},
    # {'name': 'sex', 'weight': 0.001},
    # {'name': 'age', 'weight': 0.001}
    # {'name': 'test_sex', 'weight': 1}
]
for similarity in similarities:
    predictor.add_similarity(similarity['name'], similarity['weight'])


test_diseases = [
    'BONE & JOINT DISEASE',
    # 'NEOPLASIA'
]
checker = Check(predictor, test_diseases, tolerance=0.5, threshold=0.5, verbose=True, fail_only=False)
total_deviation = 0
n = 1
# total_deviation = checker.tenth_test()
for i in range(n):
    total_deviation += checker.tenfold()
print Colors.INFO, "MAE", total_deviation/n, Colors.ENDC
# checker.run_checks()
# print sim(similarities, '24', '20')
# rel: 0.625
# inst: 1
# sex: 1
# 0.875

# In these terms, a type I error is a false positive (incorrectly choosing alternative hypothesis instead of null
# hypothesis), and a type II error is a false negative (incorrectly choosing the null hypothesis instead of the
# alternative hypothesis).